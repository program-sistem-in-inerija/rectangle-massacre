extends CharacterBody2D
class_name Player

@export var movement_speed: int = 500

@onready var collision_shape = $CollisionShape2D
@onready var weapon_manager = $WeaponManager
@onready var health_stat = $Health
@onready var team = $Team

signal health_change(new_health)
signal player_died

func _ready() -> void:
	weapon_manager.initialize(team.team)
	
func _physics_process(_delta):
	var motion := Vector2.ZERO
	
	motion.x = Input.get_action_strength("Right") - Input.get_action_strength("Left")
	motion.y = Input.get_action_strength("Down") - Input.get_action_strength("Up")
	
	motion = motion.normalized()
	set_velocity(motion * movement_speed)
	move_and_slide()
	motion = velocity
	look_at(get_global_mouse_position())
	
func get_team() -> int:
	return team.team
	
func handle_hit():
	health_stat.health -= 1
	emit_signal("health_change", health_stat.health)
	if health_stat.health <= 0:
		die()

func die():
	emit_signal("player_died")
	queue_free()

func _on_damage_detection_body_entered(body):
	if body.has_method("get_team") and body.get_team() != get_team():
		handle_hit()

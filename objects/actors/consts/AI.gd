extends Node2D

class_name AI

signal state_changed(new_state)

enum State {
	PATROL,
	STANDBY,
	ATTACK
}

var current_state: int = State.STANDBY : set = set_state


@onready var detection_zone = $DetectionRange
var player: Player = null
var actor

func init(_actor):
	self.actor = _actor

func _process(_delta):
	match current_state:
		State.STANDBY:
			pass
		State.ATTACK:
			if player != null:
				actor.rotation = actor.global_position.direction_to(player.global_position).angle()
				actor.attack(player)
				

func set_state(new_state: int):
	if new_state == current_state:
		return
	current_state = new_state
	emit_signal("state_changed", current_state)

func _on_DetectionRange_body_entered(body):
	if body.is_in_group("player"):
		set_state(State.ATTACK)
		player = body


func _on_DetectionRange_body_exited(body):
	if body.is_in_group("player"):
		set_state(State.STANDBY)
		player = null

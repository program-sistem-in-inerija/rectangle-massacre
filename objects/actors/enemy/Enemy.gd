extends CharacterBody2D

class_name Enemy

var motion = Vector2()

@onready var health_stat = $Health
@onready var team = $Team

@onready var ai = $AI

func _ready():
	ai.init(self)

func attack(body):
	position += (body.position - position) / 50
	look_at(body.position)
	move_and_collide(motion)
	
func get_team() -> int:
	return team.team

func handle_hit():
	health_stat.health -= 1;
	if health_stat.health <= 0:
		die()
		
func die():
	queue_free()

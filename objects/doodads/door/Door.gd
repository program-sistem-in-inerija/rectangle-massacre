extends Area2D

@export_file var NEXT_LEVEL

func _on_Door_body_entered(body):
	if body.is_in_group("player") and NEXT_LEVEL != "":
		var key = NEXT_LEVEL.get_file()
		key = key.left(key.length() - 5)
		AutoloadConfig.set_config("completion", key, true)
		get_tree().change_scene_to_file(NEXT_LEVEL)

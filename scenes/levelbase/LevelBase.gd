extends Node2D

@onready var gui = $GUI
@onready var player = $Player
@onready var bullet_manager = $BulletManager

const GameOverScreen = preload("res://scenes/gui/GameOver.tscn")
const PauseScreen = preload("res://scenes/gui/PauseScreen.tscn")

func _ready():
	gui.set_player(player)
	player.connect("player_died",Callable(self,"handle_player_death"))
	GlobalSignals.connect("bullet_fired",Callable(bullet_manager,"handle_bullet_spawned"))

func handle_player_death():
	var game_over = GameOverScreen.instantiate()
	add_child(game_over)

func _unhandled_input(event):
	if event.is_action_pressed("Pause"):
		var pause_screen = PauseScreen.instantiate()
		add_child(pause_screen)

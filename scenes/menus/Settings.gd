extends Control

@export_file var mainMenu

@onready var buttonScript = load("res://scenes/menus/nodes/KeyButton.gd")
@onready var keyContainer = get_node("MarginContainer/VBoxContainer/KeyContainer")

var buttons

func _ready():
	for key in AutoloadConfig._config_file.get_section_keys("controls"):
		var hbox = HBoxContainer.new()
		var label = Label.new()
		var button = Button.new()
		var val = AutoloadConfig._config_file.get_value("controls", key)
		
		hbox.set_h_size_flags(Control.SIZE_EXPAND_FILL)
		label.set_h_size_flags(Control.SIZE_EXPAND_FILL)
		label.add_theme_font_override("font", load("res://Pixelboy_Font.ttf"))
		button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
		
		label.text = key
		button.text = val
		
		button.set_script(buttonScript)
		button.key = key
		button.value = val
		button.menu = self
		button.toggle_mode = true
		button.focus_mode = Control.FOCUS_NONE
		
		hbox.add_child(label)
		hbox.add_child(button)
		keyContainer.add_child(hbox)

func _on_Button_pressed():
	if ResourceLoader.exists(mainMenu):
		var _error = get_tree().change_scene_to_file(mainMenu)
		
func change_bind(key, value):
	AutoloadConfig.set_config("controls", key, value)
	AutoloadConfig.setInput(key, value)

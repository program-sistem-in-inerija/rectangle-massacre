extends Control

@export_file var mainMenu

func _on_main_menu_pressed():
	if ResourceLoader.exists(mainMenu):
		var _error = get_tree().change_scene_to_file(mainMenu)

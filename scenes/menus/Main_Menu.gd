extends Control

@export var levelSelectScene: PackedScene
@export var settingScene: PackedScene

func _ready():
	$MarginContainer/VBoxContainer/VBoxContainer/MarginContainer/StartButton.grab_focus()

func _on_StartButton_pressed():
	get_tree().change_scene_to_file(levelSelectScene.resource_path)

func _on_OptionButton_pressed():
	get_tree().change_scene_to_file(settingScene.resource_path)

func _on_QuitButton_pressed():
	get_tree().quit()

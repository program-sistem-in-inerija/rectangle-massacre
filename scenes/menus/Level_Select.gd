extends Control

@export_file var mainMenu
@export_dir var dir_path

const LEVEL_BTN = preload("res://scenes/menus/nodes/Level_Button.tscn")

var array = []

@onready var grid = $MarginContainer/VBoxContainer/GridContainer

func _ready():
	for key in AutoloadConfig._config_file.get_section_keys("completion"):
		var val = AutoloadConfig._config_file.get_value("completion", key)
		array.push_back(val)
	get_levels(dir_path)

func get_levels(path):
	var index = 0
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var file_name
		while true:
			file_name = dir.get_next()
			if file_name == "":
				break
			elif not file_name.begins_with("."):
				create_level_button('%s/%s' % [dir.get_current_dir(), file_name], file_name, array[index])
				index += 1
		dir.list_dir_end()
	else:
		print("An error occured when trying to access the dir path!")

func create_level_button(level_path: String, level_name: String, enabled: bool):
	var btn = LEVEL_BTN.instantiate()
	var name = level_name
	name = name.replace('.remap', '')
	name = name.replace('.tscn', '')
	btn.text = name.replace('_', ' ')
	btn.path = level_path.replace('.remap', '')
	if enabled == false:
		btn.set_disabled(true)
	grid.add_child(btn)


func _on_Button_pressed():
	if ResourceLoader.exists(mainMenu):
		var _error = get_tree().change_scene_to_file(mainMenu)

extends Button

@export var path: String = "" # (String, FILE)

func _on_Level_Button_pressed():
	if path == null:
		return
	if path == "":
		return
	get_tree().change_scene_to_file(path)

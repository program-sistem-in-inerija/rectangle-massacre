extends Button

var key
var value
var menu

var waitingInput = false

func _input(event):
	if waitingInput:
		if event is InputEventKey:
			value = event.keycode
			text = OS.get_keycode_string(value)
			menu.change_bind(key, text)
			waitingInput = false
		if event is InputEventMouseButton:
			text = OS.get_keycode_string(value)
			waitingInput = false

func _toggled(pressed):
	if pressed:
		waitingInput = true
		set_text("Press Any Key")

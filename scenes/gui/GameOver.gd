extends CanvasLayer

@export_file var mainMenu

func _ready():
	get_tree().paused = true

func _on_RestartButton_pressed():
	get_tree().paused = false
	queue_free()
	get_tree().reload_current_scene()

func _on_QuitButton_pressed():
	if ResourceLoader.exists(mainMenu):
		get_tree().paused = false
		var _error = get_tree().change_scene_to_file(mainMenu)

extends CanvasLayer

@onready var health_bar = $MarginContainer/Rows/TopRow/CenterContainer/HealthBar

var player: Player

func set_player(_player: Player):
	self.player = _player
	set_new_health_value(self.player.health_stat.health)
	self.player.connect("health_change",Callable(self,"set_new_health_value"))

func set_new_health_value(new_health: int):
	health_bar.value = new_health

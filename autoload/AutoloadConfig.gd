extends Node

const CONFIG_PATH = "res://config.cfg"

var _config_file = ConfigFile.new()

var _default = {
	"window": {
		"Width": 720,
		"Height": 720
	},
	"controls": {
		"Up": "W",
		"Down": "S",
		"Left": "A",
		"Right": "D",
		"Pause": "Escape"
	},
	"completion": {
		"Level_1": true,
		"Level_2": false,
		"Level_3": false,
		"Level_4": false,
		"Level_5": false,
		"Level_6": false,
	}
}

var screenSize: Vector2

func _ready():
	save_config()
	load_config()
	setInput("Up", get_config("controls", "Up"))
	setInput("Down", get_config("controls", "Down"))
	setInput("Left", get_config("controls", "Left"))
	setInput("Right", get_config("controls", "Right"))
	setInput("Pause", get_config("controls", "Pause"))
	screenSize = Vector2(_config_file.get_value("window", "width", get_window().get_size().x),
	_config_file.get_value("window", "height", get_window().get_size().y))
	get_window().size = screenSize

func save_config():
	var error = _config_file.load(CONFIG_PATH)
	if error == OK:
		return
	for section in _default.keys():
		for key in _default[section].keys():
			_config_file.set_value(section, key, _default[section][key])
	_config_file.save(CONFIG_PATH)
	
func load_config():
	var error = _config_file.load(CONFIG_PATH)
	if error != OK:
		print("Error loading the settings, Error code: %s" % error)
		return []
	
	var values = []
	for section in _config_file.get_sections():
		for key in _config_file.get_section_keys(section):
			var val = _config_file.get_value(section, key)
			values.append(_config_file.get_value(section, key, val))
	
	return values
	
func get_config(category, key):
	return _config_file.get_value(category, key)
	
func set_config(category, key, value):
	_config_file.set_value(category, key, value)
	_config_file.save(CONFIG_PATH)
	
func setInput(inputName, key):
	var new_input = InputEventKey.new()
	new_input.set_keycode(OS.find_keycode_from_string(key))
	if !InputMap.has_action(inputName):
		InputMap.add_action(inputName)
	
	InputMap.action_erase_events(inputName)
	InputMap.action_add_event(inputName, new_input)
